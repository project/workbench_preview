<?php

/**
 * @file
 * Workbench Preview views handler.
 */

/**
 * Implements hook_views_data_alter().
 */
function workbench_preview_views_data_alter(&$data) {
  $data['node_revision']['revisioned_content_filter'] = array(
    'title' => 'Workbench Preview revision filter',
    'help' => 'Display the latest content revision to privileged users and only published content for others.',
    'filter' => array(
      'type' => 'enabled-disabled',
      'handler' => 'workbench_preview_handler_filter_revisioned_content',
    ),
  );
}

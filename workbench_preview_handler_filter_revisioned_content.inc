<?php

/**
 * @file
 * Definition of workbench_preview_handler_filter_revisioned_content.
 */

/**
 * Filter handler which displays the latest revisioned content.
 *
 * If user does not have the correct permission, only show published content.
 *
 * @ingroup views_field_handlers
 */
class workbench_preview_handler_filter_revisioned_content extends views_handler_filter_boolean_operator {

  /**
   * Alter the default text for this class.
   */
  public function construct() {
    parent::construct();
    $this->value_value = t('Enable filter?');
  }

  /**
   * Add 'where' condition to the views query depending on the user permission.
   */
  public function query() {
    // Do not do anything if the filter isn't enabled.
    if ($this->value == TRUE) {
      // Add join to workbench_moderation_node_history table.
      $join = new views_join();
      $join->table = 'workbench_moderation_node_history';
      $join->field = 'vid';
      $join->left_table = 'node_revision';
      $join->left_field = 'vid';
      $join->type = 'LEFT';
      $this->query->add_relationship('workbench_moderation_node_history', $join, 'node');

      if (user_access('view latest revisioned content')) {
        // Add moderation current filter to show the latest revision of content.
        $this->query->add_where($this->options['group'], "workbench_moderation_node_history.is_current", $this->value, '=');
      }
      else {
        // Show latest published version to any user who do not have permission.
        $this->query->add_where($this->options['group'], "workbench_moderation_node_history.published", 1, '=');
      }
    }
  }

  /**
   * Set the default option to enable in the filter.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['value']['default'] = TRUE;

    return $options;
  }


  /**
   * Hide expose button.
   */
  public function show_expose_button(&$form, &$form_state) {
    parent::show_expose_button($form, $form_state);
    // Remove the expose to visitors checkbox.
    unset($form['expose_button']);
    return $form;
  }

}

/**
 * @file
 * README file for Workbench Preview.
 */

Workbench Preview
Preview draft content in it's context while unauthorised users only see
published content.

----

1.  Introduction

This little module provides a views handler that allows privileged users to view
the latest version of content in views across the site, whereas only the
published versions will be shown to users without.

It is controlled by a permission so that only users with this are allowed to see
the revised content. This allows content editors to see the content exactly
how it will look in views across the site before the content is actually
published and viewable by other users. There is no longer a need for content to
be added on stager to see how it will look, and then again on production.

----

2. Installatioon

It is presumed that your site already has a workbench setup for content
workflow. If not, follow their documentation to get setup.

Once you have enabled this module, you'll need to configure a view and user
permission.

----

3. Configuration

Grant the permission provided by this module ('View the latest revision of
content') to users who are allowed to see the draft content, such as content
editors / content admins. Users who do not have this permission will only see
published content from any views with the filter.

Edit an existing 'Content revisions' view or create a new one of this type. Make
sure that the "Content revision: Content" relationship is the one using the nid
table rather than the one using vid. Add a new filter criteria to the view and
chose 'Workbench Preview revision filter'. A list of moderation states will be
displayed for you to chose from. Those selected here will appear in the view for
authorised users, whereas all other users will see only the published verson of
content in this view. Continue to tweaks the view for the content types etc. you
want to display.

You can also use the template provided by this module to help get you going. It
will create a view with the filter and the correct relationship, then you can
tweak it for the content you want to display.
